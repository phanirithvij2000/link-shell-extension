
D:\data\cpp\hardlinks\ln>..\Tools\where.exe  *.* test\UNCSimple   | sort
10 matching file(s) found, using 3001 bytes.
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\_F\0_A
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\_F\1_A
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\_F\1_C
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\Folder0\0_A
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\Folder0\0_B
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\Folder0\0_C
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\Folder0\0_D
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\Folder1\1_A
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\Folder1\1_B
D:\data\cpp\hardlinks\ln\test\UNCSimple\source\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --copy UNC to UNC via Localhost 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy \\quadtatz\UNCsimple\source \\quadtatz\UNCsimple\dest  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.001     3.001         0         0         0         0
    File:         7         7         0         0         0         0
   Times:        00:00:00.002
  Folder:         5         5         -         0         0         0
 Symlink:         3         -         3         0         0         0
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F\F0
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder1
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_A
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_B
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_C
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_D
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder1\1_A
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder1\1_B
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder1\1_C
+s D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F\0_A
+s D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F\1_A
+s D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F\1_C
Junction:         0         -         0         0         0         0
ErrorLevel == 0
test\UNCSimple\dest\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\dest\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\dest\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --delorean UNC to UNC 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --delorean \\quadtatz\UNCsimple\source \\quadtatz\UNCsimple\dest \\quadtatz\UNCsimple\bk1  1>sortout 


-d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\F0
-f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_D
                      Overall               Clone              Mirror
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     2.469         0         0     2.469       532         0         0
    File:         6         0         0         6         1         0         0
   Times:        00:00:00.002        00:00:00.007        00:00:00.034
  Folder:         4         0         -         4         1         0         0
 Symlink:         3         -         0         3         0         0         0
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_A
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_B
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_C
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_A
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_B
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_C
=s D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\0_A
=s D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\1_A
=s D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\1_C
Junction:         0         -         0         0         0         0         0
ErrorLevel == 0
test\UNCSimple\bk1\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\bk1\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\bk1\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --copy Drive to UNC via Localhost 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\UNCSimple\source \\quadtatz\UNCsimple\dest  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.001     3.001         0         0         0         0
    File:         7         7         0         0         0         0
   Times:        00:00:00.002
  Folder:         5         5         -         0         0         0
 Symlink:         3         -         3         0         0         0
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F\F0
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0
+d D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder1
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_A
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_B
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_C
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_D
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder1\1_A
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder1\1_B
+f D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder1\1_C
+s D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F\0_A
+s D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F\1_A
+s D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\_F\1_C
Junction:         0         -         0         0         0         0
ErrorLevel == 0
test\UNCSimple\dest\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\dest\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\dest\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --delorean Dr�ve to UNC via Localhost 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --delorean test\UNCSimple\source \\quadtatz\UNCsimple\dest \\quadtatz\UNCsimple\bk1  1>sortout 


-d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\F0
-f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_D
                      Overall               Clone              Mirror
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     2.469         0         0     2.469       532         0         0
    File:         6         0         0         6         1         0         0
   Times:        00:00:00.002        00:00:00.007        00:00:00.034
  Folder:         4         0         -         4         1         0         0
 Symlink:         3         -         0         3         0         0         0
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_A
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_B
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_C
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_A
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_B
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_C
=s D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\0_A
=s D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\1_A
=s D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\1_C
Junction:         0         -         0         0         0         0         0
ErrorLevel == 0
test\UNCSimple\bk1\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\bk1\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\bk1\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --copy UNC to UNC via sharename, nolocaluncresolve 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --nolocaluncresolve --copy \\quadtatz\UNCsimple\source \\quadtatz\UNCsimple\dest  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.001     3.001         0         0         0         0
    File:         7         7         0         0         0         0
   Times:        00:00:00.002
  Folder:         5         5         -         0         0         0
 Symlink:         3         -         3         0         0         0
+d \\quadtatz\UNCsimple\dest
+d \\quadtatz\UNCsimple\dest\_F
+d \\quadtatz\UNCsimple\dest\_F\F0
+d \\quadtatz\UNCsimple\dest\Folder0
+d \\quadtatz\UNCsimple\dest\Folder1
+f \\quadtatz\UNCsimple\dest\Folder0\0_A
+f \\quadtatz\UNCsimple\dest\Folder0\0_B
+f \\quadtatz\UNCsimple\dest\Folder0\0_C
+f \\quadtatz\UNCsimple\dest\Folder0\0_D
+f \\quadtatz\UNCsimple\dest\Folder1\1_A
+f \\quadtatz\UNCsimple\dest\Folder1\1_B
+f \\quadtatz\UNCsimple\dest\Folder1\1_C
+s \\quadtatz\UNCsimple\dest\_F\0_A
+s \\quadtatz\UNCsimple\dest\_F\1_A
+s \\quadtatz\UNCsimple\dest\_F\1_C
Junction:         0         -         0         0         0         0
ErrorLevel == 0
test\UNCSimple\dest\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\dest\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\dest\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --delorean UNC to UNC via sharename, nolocaluncresolve 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --nolocaluncresolve --delorean \\quadtatz\UNCsimple\source \\quadtatz\UNCsimple\dest \\quadtatz\UNCsimple\bk1  1>sortout 


'Sleeping 5 sec: Destination is a remote drive.
-d \\quadtatz\UNCsimple\bk1\_F\F0
-f \\quadtatz\UNCsimple\bk1\Folder0\0_D
                      Overall               Clone              Mirror
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     2.469         0         0     2.469       532         0         0
    File:         6         0         0         6         1         0         0
   Times:        00:00:00.002        00:00:00.007        00:00:00.034
  Folder:         4         0         -         4         1         0         0
 Symlink:         3         -         0         3         0         0         0
=d \\quadtatz\UNCsimple\bk1
=d \\quadtatz\UNCsimple\bk1\_F
=d \\quadtatz\UNCsimple\bk1\Folder0
=d \\quadtatz\UNCsimple\bk1\Folder1
=f \\quadtatz\UNCsimple\bk1\Folder0\0_A
=f \\quadtatz\UNCsimple\bk1\Folder0\0_B
=f \\quadtatz\UNCsimple\bk1\Folder0\0_C
=f \\quadtatz\UNCsimple\bk1\Folder1\1_A
=f \\quadtatz\UNCsimple\bk1\Folder1\1_B
=f \\quadtatz\UNCsimple\bk1\Folder1\1_C
=s \\quadtatz\UNCsimple\bk1\_F\0_A
=s \\quadtatz\UNCsimple\bk1\_F\1_A
=s \\quadtatz\UNCsimple\bk1\_F\1_C
Junction:         0         -         0         0         0         0         0
ErrorLevel == 0
test\UNCSimple\bk1\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\bk1\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\bk1\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --copy Drive to UNC via sharename, nolocaluncresolve 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --nolocaluncresolve --copy test\UNCSimple\source \\quadtatz\UNCsimple\dest  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.001     3.001         0         0         0         0
    File:         7         7         0         0         0         0
   Times:        00:00:00.002
  Folder:         5         5         -         0         0         0
 Symlink:         3         -         3         0         0         0
+d \\quadtatz\UNCsimple\dest
+d \\quadtatz\UNCsimple\dest\_F
+d \\quadtatz\UNCsimple\dest\_F\F0
+d \\quadtatz\UNCsimple\dest\Folder0
+d \\quadtatz\UNCsimple\dest\Folder1
+f \\quadtatz\UNCsimple\dest\Folder0\0_A
+f \\quadtatz\UNCsimple\dest\Folder0\0_B
+f \\quadtatz\UNCsimple\dest\Folder0\0_C
+f \\quadtatz\UNCsimple\dest\Folder0\0_D
+f \\quadtatz\UNCsimple\dest\Folder1\1_A
+f \\quadtatz\UNCsimple\dest\Folder1\1_B
+f \\quadtatz\UNCsimple\dest\Folder1\1_C
+s \\quadtatz\UNCsimple\dest\_F\0_A
+s \\quadtatz\UNCsimple\dest\_F\1_A
+s \\quadtatz\UNCsimple\dest\_F\1_C
Junction:         0         -         0         0         0         0
ErrorLevel == 0
test\UNCSimple\dest\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\dest\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\dest\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --delorean Drive to UNC via sharename, nolocaluncresolve 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --nolocaluncresolve --delorean test\UNCSimple\source \\quadtatz\UNCsimple\dest \\quadtatz\UNCsimple\bk1  1>sortout 


'Sleeping 5 sec: Destination is a remote drive.
-d \\quadtatz\UNCsimple\bk1\_F\F0
-f \\quadtatz\UNCsimple\bk1\Folder0\0_D
                      Overall               Clone              Mirror
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     2.343       406         0     1.937       532         0         0
    File:         6         1         0         5         1         0         0
   Times:        00:00:00.002        00:00:00.007        00:00:00.034
  Folder:         4         0         -         4         1         0         0
 Symlink:         3         -         0         3         0         0         0
+f \\quadtatz\UNCsimple\bk1\Folder0\0_C
=d \\quadtatz\UNCsimple\bk1
=d \\quadtatz\UNCsimple\bk1\_F
=d \\quadtatz\UNCsimple\bk1\Folder0
=d \\quadtatz\UNCsimple\bk1\Folder1
=f \\quadtatz\UNCsimple\bk1\Folder0\0_A
=f \\quadtatz\UNCsimple\bk1\Folder0\0_B
=f \\quadtatz\UNCsimple\bk1\Folder1\1_A
=f \\quadtatz\UNCsimple\bk1\Folder1\1_B
=f \\quadtatz\UNCsimple\bk1\Folder1\1_C
=s \\quadtatz\UNCsimple\bk1\_F\0_A
=s \\quadtatz\UNCsimple\bk1\_F\1_A
=s \\quadtatz\UNCsimple\bk1\_F\1_C
Junction:         0         -         0         0         0         0         0
ErrorLevel == 0
D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_C
  :                                       406
Total size: 406 bytes.
D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_C
  :                                       532
Total size: 532 bytes.
test\UNCSimple\bk1\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\bk1\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\bk1\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --delorean Drive to UNC via sharename, nolocaluncresolve, clean fails 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --nolocaluncresolve --delorean test\UNCSimple\source \\quadtatz\UNCsimple\dest \\quadtatz\UNCsimple\bk1  1>sortout 


'Sleeping 5 sec: Destination is a remote drive.
                      Overall               Clone              Mirror
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     2.469         0         0     2.469         0         0         0
    File:         6         0         0         6         0         0         0
   Times:        00:00:00.002        00:00:00.007        00:00:00.034
  Folder:         5         0         -         5         0         0         0
 Symlink:         3         -         0         3         0         0         0
!?f(0x00000005) \\quadtatz\UNCsimple\dest\Folder0\0_D
=d \\quadtatz\UNCsimple\bk1
=d \\quadtatz\UNCsimple\bk1\_F
=d \\quadtatz\UNCsimple\bk1\_F\F0
=d \\quadtatz\UNCsimple\bk1\Folder0
=d \\quadtatz\UNCsimple\bk1\Folder1
=f \\quadtatz\UNCsimple\bk1\Folder0\0_A
=f \\quadtatz\UNCsimple\bk1\Folder0\0_B
=f \\quadtatz\UNCsimple\bk1\Folder0\0_C
=f \\quadtatz\UNCsimple\bk1\Folder1\1_A
=f \\quadtatz\UNCsimple\bk1\Folder1\1_B
=f \\quadtatz\UNCsimple\bk1\Folder1\1_C
=s \\quadtatz\UNCsimple\bk1\_F\0_A
=s \\quadtatz\UNCsimple\bk1\_F\1_A
=s \\quadtatz\UNCsimple\bk1\_F\1_C
Junction:         0         -         0         0         0         0         0
WARNING: Delorean Copy finished successfully but see output for errors.
ErrorLevel == -16
test\UNCSimple\bk1\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\bk1\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\bk1\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --mirror Drive to UNC via sharename, nolocaluncresolve, clean fails 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --nolocaluncresolve --mirror test\UNCSimple\source \\quadtatz\UNCsimple\dest  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     2.469         0         0     2.469         0         0         0
    File:         6         0         0         6         0         0         0
   Times:        00:00:00.002
  Folder:         5         0         -         5         0         0         0
 Symlink:         3         -         0         3         0         0         0
=d \\quadtatz\UNCsimple\dest
=d \\quadtatz\UNCsimple\dest\_F
=d \\quadtatz\UNCsimple\dest\_F\F0
=d \\quadtatz\UNCsimple\dest\Folder0
=d \\quadtatz\UNCsimple\dest\Folder1
=f \\quadtatz\UNCsimple\dest\Folder0\0_A
=f \\quadtatz\UNCsimple\dest\Folder0\0_B
=f \\quadtatz\UNCsimple\dest\Folder0\0_C
=f \\quadtatz\UNCsimple\dest\Folder1\1_A
=f \\quadtatz\UNCsimple\dest\Folder1\1_B
=f \\quadtatz\UNCsimple\dest\Folder1\1_C
=s \\quadtatz\UNCsimple\dest\_F\0_A
=s \\quadtatz\UNCsimple\dest\_F\1_A
=s \\quadtatz\UNCsimple\dest\_F\1_C
Junction:         0         -         0         0         0         0         0
ErrorLevel == 0
test\UNCSimple\dest\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\dest\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\dest\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --copy UNC to UNC via Ip-Adresse 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy \\10.0.0.74\UNCsimple\source \\10.0.0.74\UNCsimple\dest  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.001     3.001         0         0         0         0
    File:         7         7         0         0         0         0
   Times:        00:00:00.002
  Folder:         5         5         -         0         0         0
 Symlink:         3         -         3         0         0         0
+d \\10.0.0.74\UNCsimple\dest
+d \\10.0.0.74\UNCsimple\dest\_F
+d \\10.0.0.74\UNCsimple\dest\_F\F0
+d \\10.0.0.74\UNCsimple\dest\Folder0
+d \\10.0.0.74\UNCsimple\dest\Folder1
+f \\10.0.0.74\UNCsimple\dest\Folder0\0_A
+f \\10.0.0.74\UNCsimple\dest\Folder0\0_B
+f \\10.0.0.74\UNCsimple\dest\Folder0\0_C
+f \\10.0.0.74\UNCsimple\dest\Folder0\0_D
+f \\10.0.0.74\UNCsimple\dest\Folder1\1_A
+f \\10.0.0.74\UNCsimple\dest\Folder1\1_B
+f \\10.0.0.74\UNCsimple\dest\Folder1\1_C
+s \\10.0.0.74\UNCsimple\dest\_F\0_A
+s \\10.0.0.74\UNCsimple\dest\_F\1_A
+s \\10.0.0.74\UNCsimple\dest\_F\1_C
Junction:         0         -         0         0         0         0
ErrorLevel == 0
test\UNCSimple\dest\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\dest\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\dest\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --delorean UNC to UNC via sharename, nolocaluncresolve 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --delorean \\10.0.0.74\UNCsimple\source \\quadtatz\UNCsimple\dest \\quadtatz\UNCsimple\bk1  1>sortout 


-d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\F0
-f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_D
                      Overall               Clone              Mirror
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     2.469         0         0     2.469       532         0         0
    File:         6         0         0         6         1         0         0
   Times:        00:00:00.002        00:00:00.007        00:00:00.034
  Folder:         4         0         -         4         1         0         0
 Symlink:         3         -         0         3         0         0         0
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0
=d D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_A
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_B
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_C
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_A
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_B
=f D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_C
=s D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\0_A
=s D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\1_A
=s D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\_F\1_C
Junction:         0         -         0         0         0         0         0
ErrorLevel == 0
test\UNCSimple\bk1\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\bk1\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\bk1\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --recursive UNC test 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --recursive \\10.0.0.74\UNCsimple\source \\10.0.0.74\UNCsimple\dest  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     3.001         0     3.001         0         0         0
    File:         7         0         7         0         0         0
   Times:        00:00:00.002
  Folder:         5         5         -         0         0         0
 Symlink:         3         -         3         0         0         0
+d \\10.0.0.74\UNCsimple\dest
+d \\10.0.0.74\UNCsimple\dest\_F
+d \\10.0.0.74\UNCsimple\dest\_F\F0
+d \\10.0.0.74\UNCsimple\dest\Folder0
+d \\10.0.0.74\UNCsimple\dest\Folder1
+h \\10.0.0.74\UNCsimple\dest\Folder0\0_A
+h \\10.0.0.74\UNCsimple\dest\Folder0\0_B
+h \\10.0.0.74\UNCsimple\dest\Folder0\0_C
+h \\10.0.0.74\UNCsimple\dest\Folder0\0_D
+h \\10.0.0.74\UNCsimple\dest\Folder1\1_A
+h \\10.0.0.74\UNCsimple\dest\Folder1\1_B
+h \\10.0.0.74\UNCsimple\dest\Folder1\1_C
+s \\10.0.0.74\UNCsimple\dest\_F\0_A
+s \\10.0.0.74\UNCsimple\dest\_F\1_A
+s \\10.0.0.74\UNCsimple\dest\_F\1_C
Junction:         0         -         0         0         0         0
ErrorLevel == 0
test\UNCSimple\dest\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\dest\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\dest\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --copy UNC test with noexistant share 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy \\10.0.0.74\UNCsimple\source \\10.0.0.74\UNCsimple_notthere\dest  1>sortout 
ERROR: Source directory '\\10.0.0.74\UNCsimple_notthere\dest' does not exist
ErrorLevel == -1

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --delorean UNC  

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --delorean \\10.0.0.74\UNCsimple\source \\10.0.0.74\UNCsimple\dest \\10.0.0.74\UNCsimple\bk1  1>sortout 


'Sleeping 5 sec: Destination is a remote drive.
                      Overall               Clone              Mirror
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     3.001     1.934         0     1.067         0         0         0
    File:         7         4         0         3         0         0         0
   Times:        00:00:00.002        00:00:00.007        00:00:00.034
  Folder:         5         1         -         4         0         0         0
 Symlink:         3         -         0         3         0         0         0
+d \\10.0.0.74\UNCsimple\bk1\Folder0
+f \\10.0.0.74\UNCsimple\bk1\Folder0\0_A
+f \\10.0.0.74\UNCsimple\bk1\Folder0\0_B
+f \\10.0.0.74\UNCsimple\bk1\Folder0\0_C
+f \\10.0.0.74\UNCsimple\bk1\Folder0\0_D
=d \\10.0.0.74\UNCsimple\bk1
=d \\10.0.0.74\UNCsimple\bk1\_F
=d \\10.0.0.74\UNCsimple\bk1\_F\F0
=d \\10.0.0.74\UNCsimple\bk1\Folder1
=f \\10.0.0.74\UNCsimple\bk1\Folder1\1_A
=f \\10.0.0.74\UNCsimple\bk1\Folder1\1_B
=f \\10.0.0.74\UNCsimple\bk1\Folder1\1_C
=s \\10.0.0.74\UNCsimple\bk1\_F\0_A
=s \\10.0.0.74\UNCsimple\bk1\_F\1_A
=s \\10.0.0.74\UNCsimple\bk1\_F\1_C
Junction:         0         -         0         0         0         0         0
ErrorLevel == 0
test\UNCSimple\bk1\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\bk1\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\bk1\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --delorean \\10.0.0.74\UNCsimple\source \\10.0.0.74\UNCsimple\dest \\10.0.0.74\UNCsimple\bk1  1>sortout 


'Sleeping 5 sec: Destination is a remote drive.
-f \\10.0.0.74\UNCsimple\bk1\Folder1\1_C
                      Overall               Clone              Mirror
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     2.469     1.934         0       535       532         0         0
    File:         6         4         0         2         1         0         0
   Times:        00:00:00.002        00:00:00.007        00:00:00.034
  Folder:         5         1         -         4         0         0         0
 Symlink:         3         -         0         3         0         0         0
+d \\10.0.0.74\UNCsimple\bk1\Folder0
+f \\10.0.0.74\UNCsimple\bk1\Folder0\0_A
+f \\10.0.0.74\UNCsimple\bk1\Folder0\0_B
+f \\10.0.0.74\UNCsimple\bk1\Folder0\0_C
+f \\10.0.0.74\UNCsimple\bk1\Folder0\0_D
=d \\10.0.0.74\UNCsimple\bk1
=d \\10.0.0.74\UNCsimple\bk1\_F
=d \\10.0.0.74\UNCsimple\bk1\_F\F0
=d \\10.0.0.74\UNCsimple\bk1\Folder1
=f \\10.0.0.74\UNCsimple\bk1\Folder1\1_A
=f \\10.0.0.74\UNCsimple\bk1\Folder1\1_B
=s \\10.0.0.74\UNCsimple\bk1\_F\0_A
=s \\10.0.0.74\UNCsimple\bk1\_F\1_A
=s \\10.0.0.74\UNCsimple\bk1\_F\1_C
Junction:         0         -         0         0         0         0         0
ErrorLevel == 0
test\UNCSimple\dest\_F\0_A -#-> ..\Folder0\0_A (\\?\D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_A)
test\UNCSimple\dest\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\dest\_F\1_C -> ..\Folder1\1_C

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --copy Drive to Mapped Drive 

D:\data\cpp\hardlinks\ln>REM
test\UNCSimple\source\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\source\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\source\_F\1_C -#-> ..\Folder1\1_C (\\?\D:\data\cpp\hardlinks\ln\test\UNCSimple\source\Folder1\1_C)

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --copy test\UNCSimple\source t:\dest  1>sortout 


                      Overall
              Total    Copied    Linked   Skipped  Excluded    Failed
    Byte:     2.469     2.469         0         0         0         0
    File:         6         6         0         0         0         0
   Times:        00:00:00.002
  Folder:         5         5         -         0         0         0
 Symlink:         3         -         3         0         0         0
&s t:\dest\_F\1_C
+d t:\dest
+d t:\dest\_F
+d t:\dest\_F\F0
+d t:\dest\Folder0
+d t:\dest\Folder1
+f t:\dest\Folder0\0_A
+f t:\dest\Folder0\0_B
+f t:\dest\Folder0\0_C
+f t:\dest\Folder0\0_D
+f t:\dest\Folder1\1_A
+f t:\dest\Folder1\1_B
+s t:\dest\_F\0_A
+s t:\dest\_F\1_A
Junction:         0         -         0         0         0         0
ErrorLevel == 0
test\UNCSimple\dest\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\dest\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\dest\_F\1_C -#-> ..\Folder1\1_C (\\?\D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder1\1_C)

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --delorean Drive to Mapped Drive 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --delorean test\UNCSimple\source t:\dest t:\bk1  1>sortout 


'Sleeping 5 sec: Destination is a remote drive.
-d t:\bk1\_F\F0
-f t:\bk1\Folder0\0_D
                      Overall               Clone              Mirror
              Total    Copied    Linked   Skipped   Removed  Excluded    Failed
    Byte:     1.811       406         0     1.405       532         0         0
    File:         5         1         0         4         1         0         0
   Times:        00:00:00.002        00:00:00.007        00:00:00.034
  Folder:         4         0         -         4         1         0         0
 Symlink:         3         -         0         3         0         0         0
+f t:\bk1\Folder0\0_C
=d t:\bk1
=d t:\bk1\_F
=d t:\bk1\Folder0
=d t:\bk1\Folder1
=f t:\bk1\Folder0\0_A
=f t:\bk1\Folder0\0_B
=f t:\bk1\Folder1\1_A
=f t:\bk1\Folder1\1_B
=s t:\bk1\_F\0_A
=s t:\bk1\_F\1_A
=s t:\bk1\_F\1_C
Junction:         0         -         0         0         0         0         0
ErrorLevel == 0
D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder0\0_C
  :                                       406
Total size: 406 bytes.
D:\data\cpp\hardlinks\ln\test\UNCSimple\dest\Folder0\0_C
  :                                       532
Total size: 532 bytes.
test\UNCSimple\bk1\_F\0_A -> ..\Folder0\0_A
test\UNCSimple\bk1\_F\1_A -> ..\Folder1\1_A
test\UNCSimple\bk1\_F\1_C -#-> ..\Folder1\1_C (\\?\D:\data\cpp\hardlinks\ln\test\UNCSimple\bk1\Folder1\1_C)

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>REM --probefs for UNC names & Drives 

D:\data\cpp\hardlinks\ln>REM

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --probefs \\10.0.0.74\UNCsimple 
NTFS

D:\data\cpp\hardlinks\ln>..\bin\X64\Release\ln.exe --automated_test --probefs \\10.0.0.74\UNCsimple 
NTFS
